package queue

import "strconv"

// ObjectType represents the type of a queue object
type ObjectType string

const (
	// ObjectTypeSite is the type for the Site sync objects
	ObjectTypeSite ObjectType = "site"

	// ObjectTypeSiteLocations is the type for the Site sync objects
	ObjectTypeSiteLocations ObjectType = "site_locations"

	// ObjectTypeSitesStaff is the type for the Site staff sync objects
	ObjectTypeSitesStaff ObjectType = "sites_staff"

	// ObjectTypeSitesClients is the type for the Site clients sync objects
	ObjectTypeSitesClients ObjectType = "sites_clients"

	// ObjectTypeSiteClassSchedules is the type for the Site class schedules sync objects
	ObjectTypeSiteClassSchedules ObjectType = "site_class_schedules"

	// ObjectTypeAccountClientsHistory is the type for an Acount clients history sync objects
	ObjectTypeAccountClientsHistory ObjectType = "account_clients_history"
)

// SyncMessage holds a queue message type and object
type SyncMessage struct {
	ObjectType ObjectType `json:"objectType"`
	Object     *Object    `json:"object"`
}

// Object represents the sync object for the queue object
type Object struct {
	ID string `json:"id"`
}

// NewSyncMessage creates a new intance of a SyncMessage
func NewSyncMessage(objType ObjectType, id string) *SyncMessage {
	return &SyncMessage{
		ObjectType: objType,
		Object:     &Object{ID: id},
	}
}

// NewSyncSiteMessage creates a new intance of a SyncMessage for a site sync object
func NewSyncSiteMessage(uuid string) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeSite,
		Object:     &Object{ID: uuid},
	}
}

// NewSyncSiteLocationsMessage creates a new intance of a SyncMessage for a site locations sync object
func NewSyncSiteLocationsMessage(mbID int32) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeSiteLocations,
		Object:     &Object{ID: strconv.FormatInt(int64(mbID), 10)},
	}
}

// NewSyncSitesStaffMessage creates a new intance of a SyncMessage for a sites staff sync object
func NewSyncSitesStaffMessage(siteUUID string) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeSitesStaff,
		Object:     &Object{ID: siteUUID},
	}
}

// NewSyncSitesClientsMessage creates a new intance of a SyncMessage for a sites clients sync object
func NewSyncSitesClientsMessage(siteUUID string) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeSitesClients,
		Object:     &Object{ID: siteUUID},
	}
}

// NewSyncSiteClassSchedules creates a new intance of a SyncMessage for a sites class schedules sync object
func NewSyncSiteClassSchedules(siteUUID string) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeSiteClassSchedules,
		Object:     &Object{ID: siteUUID},
	}
}

// NewSyncAccountClientsHistoryMessage creates a new intance of a SyncMessage for an account clients history sync object
func NewSyncAccountClientsHistoryMessage(uuid string) *SyncMessage {
	return &SyncMessage{
		ObjectType: ObjectTypeAccountClientsHistory,
		Object:     &Object{ID: uuid},
	}
}

// ReindexStatus represents the status of a reindex message
type ReindexStatus string

const (
	// ReindexStatusStart is the start status for a reindex subject
	ReindexStatusStart ReindexStatus = "start"

	// ReindexStatusDone is the done status for a reindex subject
	ReindexStatusDone ReindexStatus = "done"
)

// ReindexMessage holds a queue message type and object
type ReindexMessage struct {
	ObjectType ObjectType    `json:"objectType"`
	Object     *Object       `json:"object"`
	IndexName  string        `json:"indexName"`
	Status     ReindexStatus `json:"status"`
}

// NewReindexSiteMessage creates a new intance of a ReindexMessage for a site sync object
func NewReindexSiteMessage(uuid string, indexName string, status ReindexStatus) *ReindexMessage {
	return &ReindexMessage{
		ObjectType: ObjectTypeSite,
		Object:     &Object{ID: uuid},
		IndexName:  indexName,
		Status:     status,
	}
}

// NewReindexSiteLocationsMessage creates a new intance of a ReindexMessage for a site locations sync object
func NewReindexSiteLocationsMessage(uuid string, indexName string, status ReindexStatus) *ReindexMessage {
	return &ReindexMessage{
		ObjectType: ObjectTypeSiteLocations,
		Object:     &Object{ID: uuid},
		IndexName:  indexName,
		Status:     status,
	}
}

// NewReindexSiteStaffMessage creates a new intance of a ReindexMessage for a site staff sync object
func NewReindexSiteStaffMessage(uuid string, indexName string, status ReindexStatus) *ReindexMessage {
	return &ReindexMessage{
		ObjectType: ObjectTypeSitesStaff,
		Object:     &Object{ID: uuid},
		IndexName:  indexName,
		Status:     status,
	}
}

// NewReindexSiteClassSchedulesMessage creates a new intance of a ReindexMessage for a site class schedules sync object
func NewReindexSiteClassSchedulesMessage(uuid string, indexName string, status ReindexStatus) *ReindexMessage {
	return &ReindexMessage{
		ObjectType: ObjectTypeSiteClassSchedules,
		Object:     &Object{ID: uuid},
		IndexName:  indexName,
		Status:     status,
	}
}
