// Package healthcheck provides an easy to use healtcheck handler for HTTP requests.
package healthcheck

import (
	"encoding/json"
	"net/http"
)

const (
	// Healthpath identifies the URL for the healtcheck
	Healthpath string = "/health"
)

// Handler processes the given request
func Handler(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok"})
	res.Write([]byte(msg))
	return
}
