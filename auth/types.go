package auth

import "time"

// Config holds the configuration needed for authentication
type Config struct {
	JWTSigningKey string `toml:"jwt_signing_key"`
}

// AccountPermission holds a permission
type AccountPermission struct {
	SiteID int32
	Code   string
}

// Token represents an authentication token
type Token struct {
	ClientID           int32
	ExpiresAt          time.Time
	AccountUUID        string
	AccountPermissions []*AccountPermission
}
