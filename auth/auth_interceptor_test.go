package auth

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/Sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"
)

func TestContextWithAuthToken(t *testing.T) {
	assert := assert.New(t)

	signingKey := "randomkey"

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(10 * time.Second).Unix()
	claims["client_id"] = 12345
	claims["account_uuid"] = "1234567890123456"
	claims["account_permissions"] = []*AccountPermission{
		&AccountPermission{SiteID: 1, Code: "foo"},
		&AccountPermission{SiteID: 0, Code: "bar"},
	}
	ss, err := token.SignedString([]byte(signingKey))
	assert.Nil(err)

	ctx := metadata.NewIncomingContext(context.Background(), metadata.Pairs("authorization", "Bearer "+ss))

	// Create the interceptor
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard
	interceptor := NewInterceptor(logger, signingKey)

	// Test the method here!
	newCtx := interceptor.ContextWithAuthToken(ctx)
	assert.NotNil(newCtx)

	authToken, ok := newCtx.Value(TokenContextKey).(*Token)
	assert.True(ok)
	assert.NotNil(authToken)
	assert.Len(authToken.AccountPermissions, 2)
	assert.Equal(int32(12345), authToken.ClientID)
}

func TestExtractJWTClaims(t *testing.T) {
	assert := assert.New(t)

	signingKey := "randomkey"

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(10 * time.Second).Unix()
	claims["client_id"] = 12345
	claims["account_uuid"] = "1234567890123456"
	claims["account_permissions"] = []*AccountPermission{
		&AccountPermission{SiteID: 1, Code: "foo"},
		&AccountPermission{SiteID: 0, Code: "bar"},
	}
	ss, err := token.SignedString([]byte(signingKey))
	assert.Nil(err)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard
	interceptor := NewInterceptor(logger, signingKey)

	exClaims, err := interceptor.ExtractJWTClaims(ss)
	assert.Nil(err)

	assert.Equal(12345, int(exClaims["client_id"].(float64)))
	assert.Len(exClaims["account_permissions"], 2)
}

func TestGetAuthTokenFromContext(t *testing.T) {
	assert := assert.New(t)

	token := &Token{
		ClientID:    1234,
		AccountUUID: "1234567890123456",
		AccountPermissions: []*AccountPermission{
			&AccountPermission{SiteID: 1, Code: "foo"},
			&AccountPermission{SiteID: 0, Code: "bar"},
		},
	}
	ctx := context.WithValue(context.Background(), TokenContextKey, token)

	resultToken := GetAuthTokenFromContext(ctx)
	assert.NotNil(resultToken)
	assert.Len(resultToken.AccountPermissions, 2)
}

func TestHasPermission(t *testing.T) {
	assert := assert.New(t)

	token := &Token{
		ClientID:    1234,
		AccountUUID: "1234567890123456",
		AccountPermissions: []*AccountPermission{
			&AccountPermission{SiteID: 1, Code: "foo"},
			&AccountPermission{SiteID: 0, Code: "bar"},
		},
	}
	ctx := context.WithValue(context.Background(), TokenContextKey, token)

	r := HasPermission(ctx, 1, "foo")
	assert.True(r)

	r = HasPermission(ctx, 1, "nope")
	assert.False(r)
}
