package auth

import (
	"fmt"

	"github.com/Sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/net/context"

	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// Interceptor is the auth interceptor. If we have a JWT authorization string,
// it decodes it and sets the token on the current context.
type Interceptor struct {
	Logger        *logrus.Entry
	JWTSigningKey string
}

// ContextKey is a type for context values
type ContextKey int

const (
	// TokenContextKey is the context key for the token value
	TokenContextKey ContextKey = iota
)

// NewInterceptor creates a new AuthInterceptor
func NewInterceptor(logger *logrus.Entry, jwtSigningKey string) *Interceptor {
	return &Interceptor{
		Logger:        logger,
		JWTSigningKey: jwtSigningKey,
	}
}

// ContextWithAuthToken checks the JWT string, decrypts it, decodes it and adds
// the resulting auth Token in the context.
func (i *Interceptor) ContextWithAuthToken(ctx context.Context) context.Context {
	newCtx := ctx

	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		authorizationValues, ok := md["authorization"]
		if ok && len(md["authorization"]) == 1 {
			authValue := authorizationValues[0]

			bearerLen := len("Bearer ")
			if len(authValue) > bearerLen {
				accessToken := authorizationValues[0][bearerLen:]
				claims, err := i.ExtractJWTClaims(accessToken)
				if err != nil {
					i.Logger.Warningf("Unable to decode the access token '%s'.", accessToken)
				} else {
					// build the account permissions array
					accountPermissions := []*AccountPermission{}
					claimPermissions, ok := claims["account_permissions"].([]interface{})
					if ok {
						for _, perm := range claimPermissions {
							permission := perm.(map[string]interface{})
							accountPermissions = append(accountPermissions, &AccountPermission{
								Code:   permission["Code"].(string),
								SiteID: int32(permission["SiteID"].(float64)),
							})
						}
					}

					authToken := &Token{
						ClientID:           int32(claims["client_id"].(float64)),
						AccountUUID:        claims["account_uuid"].(string),
						AccountPermissions: accountPermissions,
						ExpiresAt:          time.Unix(int64(claims["exp"].(float64)), 0),
					}

					newCtx = context.WithValue(ctx, TokenContextKey, authToken)
				}
			} else {
				i.Logger.Warningf("Invalid access token '%s'.", authValue)
			}
		}
	}

	return newCtx
}

// UnaryServerInterceptor is a gRPC interceptor
func (i *Interceptor) UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		return handler(i.ContextWithAuthToken(ctx), req)
	}
}

// ExtractJWTClaims decrypts a JWT string and extracts the Claims
func (i *Interceptor) ExtractJWTClaims(jwtTokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(jwtTokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(i.JWTSigningKey), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}

	return nil, err
}

// GetAuthTokenFromContext gets and AuthToken from the given context
func GetAuthTokenFromContext(ctx context.Context) *Token {
	authToken, ok := ctx.Value(TokenContextKey).(*Token)
	if !ok {
		return nil
	}

	return authToken
}

// HasPermission checks if the context auth token has the given permission for the given site ID
func HasPermission(ctx context.Context, siteID int32, permission string) bool {
	authToken, ok := ctx.Value(TokenContextKey).(*Token)
	if ok {
		for _, perm := range authToken.AccountPermissions {
			if perm.SiteID == siteID && perm.Code == permission {
				return true
			}
		}
	}

	return false
}
