package logging

import (
	"testing"

	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestCloneLogrusEntry(t *testing.T) {
	logger := logrus.WithFields(logrus.Fields{"foo": "bar"})
	// setup some custom formatter
	customFormatter := new(logrus.TextFormatter)
	customFormatter.FullTimestamp = true
	logger.Logger.Formatter = customFormatter
	// set some log level
	logger.Logger.Level = logrus.DebugLevel

	newLogger := CloneLogrusEntry(logger)

	assert.Equal(t, logger.Data, newLogger.Data)
	assert.Equal(t, "bar", newLogger.Data["foo"])
	assert.Equal(t, logger.Logger.Formatter, newLogger.Logger.Formatter)
	assert.Equal(t, logger.Logger.Hooks, newLogger.Logger.Hooks)
	assert.Equal(t, logger.Logger.Level, newLogger.Logger.Level)
}
