package logging

import (
	"net"
	"testing"

	"encoding/json"

	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestFire(t *testing.T) {
	hook := &Hook{&TestConn{Testing: t}, "type"}
	logger := logrus.WithFields(logrus.Fields{})

	hook.Fire(logger)
}

// net.Conn mock
type TestConn struct {
	net.Conn
	Testing *testing.T
}

func (c *TestConn) Write(b []byte) (n int, err error) {
	var data map[string]string

	umerr := json.Unmarshal(b, &data)

	assert.Nil(c.Testing, umerr)
	assert.Contains(c.Testing, data, "@timestamp")
	assert.Contains(c.Testing, data, "message")
	assert.Contains(c.Testing, data, "level")

	return 0, nil
}
