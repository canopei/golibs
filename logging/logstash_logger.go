package logging

import (
	"github.com/Sirupsen/logrus"
)

// LogstashConfig holds the logstash configuration
type LogstashConfig struct {
	LogstashAddr string `toml:"logstash_addr"`
	LogstashType string `toml:"logstash_type"`
}

const (
	// AppDevEnvName is the name of the App dev env
	AppDevEnvName = "dev"
)

// GetLogstashLogger returns a configured logrus logger for logstash
func GetLogstashLogger(appEnv string, appName string, conf *LogstashConfig, fields logrus.Fields) *logrus.Entry {
	log := logrus.New()

	switch appEnv {
	case AppDevEnvName:
		log.Level = logrus.DebugLevel
	default:
		log.Level = logrus.InfoLevel
	}

	fields["env"] = appEnv
	fields["service"] = appName

	customFormatter := new(logrus.TextFormatter)
	customFormatter.FullTimestamp = true
	log.Formatter = customFormatter

	if conf.LogstashAddr != "" {
		hook, err := NewHook(conf.LogstashType, conf.LogstashAddr, appName)
		if err != nil {
			log.Fatal(err)
		}
		log.Hooks.Add(hook)
	}

	return log.WithFields(fields)
}
