package logging

import (
	"strings"

	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

var (
	nsqDebugLevel = nsq.LogLevelDebug.String()
	nsqInfoLevel  = nsq.LogLevelInfo.String()
	nsqWarnLevel  = nsq.LogLevelWarning.String()
	nsqErrLevel   = nsq.LogLevelError.String()
)

// NSQLogrusLogger is an adaptor between the weird go-nsq Logger and our
// standard logrus logger.
type NSQLogrusLogger struct {
	Logger *logrus.Entry
}

// NewNSQLogrusLogger returns a new NSQLogrusLogger and the current log level.
// This is a format to easily plug into nsq.SetLogger.
func NewNSQLogrusLogger(logger *logrus.Entry) NSQLogrusLogger {
	return NSQLogrusLogger{logger}
}

// Output implements stdlib log.Logger.Output using logrus
// Decodes the go-nsq log messages to figure out the log level
func (n NSQLogrusLogger) Output(_ int, s string) error {
	if len(s) > 3 {
		msg := strings.TrimSpace(s[3:])
		switch s[:3] {
		case nsqDebugLevel:
			n.Logger.Debugln(msg)
		case nsqInfoLevel:
			n.Logger.Infoln(msg)
		case nsqWarnLevel:
			n.Logger.Warnln(msg)
		case nsqErrLevel:
			n.Logger.Errorln(msg)
		default:
			n.Logger.Infoln(msg)
		}
	}
	return nil
}
