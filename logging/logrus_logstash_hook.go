package logging

import (
	"encoding/json"
	"fmt"
	"net"
	"time"

	"github.com/Sirupsen/logrus"
)

// Hook is a logrus hook implementation
type Hook struct {
	conn     net.Conn
	typeName string
}

// NewHook creates a new Hook instance
func NewHook(protocol, address, typeName string) (*Hook, error) {
	conn, err := net.Dial(protocol, address)
	if err != nil {
		return nil, err
	}
	return &Hook{conn: conn, typeName: typeName}, nil
}

func (h *Hook) format(entry *logrus.Entry) ([]byte, error) {
	fields := make(logrus.Fields)
	for k, v := range entry.Data {
		fields[k] = v
	}

	fields["@timestamp"] = entry.Time.Format(time.RFC3339Nano)
	fields["message"] = entry.Message
	fields["level"] = entry.Level.String()
	serialized, err := json.Marshal(fields)
	if err != nil {
		return nil, fmt.Errorf("Failed to marshal fields to JSON, %v", err)
	}
	return append(serialized, '\n'), nil
}

// Fire sends the buffer to logstash
func (h *Hook) Fire(entry *logrus.Entry) error {
	dataBytes, err := h.format(entry)
	if err != nil {
		return err
	}
	if _, err = h.conn.Write(dataBytes); err != nil {
		return err
	}
	return nil
}

// Levels holds the levels that this hook will kick-in for
func (h *Hook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
		logrus.InfoLevel,
		logrus.DebugLevel,
	}
}
