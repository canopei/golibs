package logging

import (
	"github.com/Sirupsen/logrus"
)

// CloneLogrusEntry clones a logrus.Entry instance
func CloneLogrusEntry(original *logrus.Entry) *logrus.Entry {
	newLogger := logrus.WithFields(original.Data)
	newLogger.Logger.Out = original.Logger.Out
	newLogger.Logger.Level = original.Logger.Level
	newLogger.Logger.Formatter = original.Logger.Formatter
	newLogger.Logger.Hooks = original.Logger.Hooks

	return newLogger
}
