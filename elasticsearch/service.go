package elasticsearch

import (
	"fmt"

	"github.com/Sirupsen/logrus"
	elastic "gopkg.in/olivere/elastic.v5"
)

const (
	// LoggerTypeValue is the type of this service log messages
	LoggerTypeValue = "ES"
)

// Service implements a ES service
type Service struct {
	Config *Config
	Client *elastic.Client
}

// NewService creates a new Service instance
func NewService(config *Config, logger *logrus.Entry) (*Service, error) {
	logger.Data["type"] = LoggerTypeValue

	client, err := elastic.NewClient(
		elastic.SetURL(fmt.Sprintf("http://%s", config.Addr)),
		elastic.SetErrorLog(logger),
		elastic.SetInfoLog(logger),
	)
	if err != nil {
		return nil, err
	}

	return &Service{
		Config: config,
		Client: client,
	}, nil
}
