package elasticsearch

// Config holds the configuration for the ES service
type Config struct {
	Addr string
}
