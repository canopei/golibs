package golibs

const (
	// HeaderRequestIDName is the name of the request ID header
	HeaderRequestIDName = "X-Request-Id"

	// HeaderGrpcRequestIDName is the name of the request ID header for gRPC rest gateway mapper
	HeaderGrpcRequestIDName = "Grpc-Metadata-request-id"

	// MetadataRequestIDName is the name of the metadata key for the request ID
	MetadataRequestIDName = "request-id"

	// LogRequestIDKeyName is the name of the key holding the request ID in the log context
	LogRequestIDKeyName = "requestid"

	// LogUserIPKeyName is the name of the key holding the user IP in the log context
	LogUserIPKeyName = "userip"
)
