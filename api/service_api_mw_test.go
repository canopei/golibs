package api

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/canopei/golibs"
	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestServiceAPIServeHTTP(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	mw := NewServiceAPIMiddleware(logger)

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true

		// Check that we have the request logger in the context
		contextLogger := req.Context().Value(RequestLoggerContextKey)
		assert.NotEmpty(contextLogger)

		requestLogger, ok := contextLogger.(*logrus.Entry)
		assert.True(ok)
		assert.Equal("reqid", requestLogger.Data[golibs.LogRequestIDKeyName])
		assert.Equal("127.0.0.1", requestLogger.Data["userip"])
	}

	req, err := http.NewRequest(http.MethodGet, "http://sng.com", nil)
	req.RemoteAddr = "127.0.0.1"
	assert.NoError(err)
	req.Header = http.Header{
		golibs.HeaderRequestIDName: []string{"reqid"},
	}
	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	// next should be always called
	assert.True(nextCalled)
	// check the response headers
	assert.NotEmpty(rec.Header().Get("Accept-Charset"))
	assert.NotEmpty(rec.Header().Get("Content-Type"))
	// Check that the request ID is passed as a gRPC header metadata
	assert.Equal("reqid", req.Header.Get(golibs.MetadataRequestIDName))
}

func TestServiceAPIServeHTTPWithMissingInfo(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	mw := NewServiceAPIMiddleware(logger)

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true

		// Check that we have the request logger in the context
		contextLogger := req.Context().Value(RequestLoggerContextKey)
		assert.NotEmpty(contextLogger)

		requestLogger := contextLogger.(*logrus.Entry)
		// Make sure these are not set if they are not available
		assert.NotContains(requestLogger.Data, golibs.LogRequestIDKeyName)
		assert.NotContains(requestLogger.Data, "userip")
	}

	req, err := http.NewRequest(http.MethodGet, "http://sng.com", nil)
	assert.NoError(err)
	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	// next should be always called
	assert.True(nextCalled)
	// check the response headers
	assert.NotEmpty(rec.Header().Get("Accept-Charset"))
	assert.NotEmpty(rec.Header().Get("Content-Type"))
	// Check that we have no request ID gRPC header
	assert.Empty(req.Header.Get(golibs.MetadataRequestIDName))
}
