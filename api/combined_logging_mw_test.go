package api

import (
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestCombinedLoggingServeHTTPWithoutRequestLogger(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	httpHandler := &dummyHandler{}
	mw := NewCombinedLoggingMiddleware(logger, httpHandler, func(out io.Writer, h http.Handler) http.Handler {
		assert.Equal(httpHandler, h)
		return h
	})

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true
	}

	req, err := http.NewRequest(http.MethodGet, "http://sng.com", nil)
	assert.NoError(err)

	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	// next should be always called
	assert.True(nextCalled)
}

func TestCombinedLoggingServeHTTPWithRequestLogger(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	httpHandler := &dummyHandler{}
	mw := NewCombinedLoggingMiddleware(logger, httpHandler, func(out io.Writer, h http.Handler) http.Handler {
		assert.Equal(httpHandler, h)
		return h
	})

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true
	}

	req, err := http.NewRequest(http.MethodGet, "http://sng.com", nil)
	assert.NoError(err)

	ctx := req.Context()
	ctx = context.WithValue(ctx, RequestLoggerContextKey, logger)
	req = req.WithContext(ctx)

	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	// next should be always called
	assert.True(nextCalled)
}

type dummyHandler struct{}

func (h dummyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Nothing
}
