package api

import (
	"context"
	"net/http"

	"bitbucket.org/canopei/golibs"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/golibs/logging"

	"github.com/Sirupsen/logrus"
)

type contextKey string

const (
	// RequestLoggerContextKey is the key name for the request logger in the request context
	RequestLoggerContextKey contextKey = "requestLogger"
)

// ServiceAPIMiddleware is a middleware that adds some details to our request / response
type ServiceAPIMiddleware struct {
	Logger *logrus.Entry
}

// NewServiceAPIMiddleware create a new ServiceAPIMiddleware
func NewServiceAPIMiddleware(logger *logrus.Entry) *ServiceAPIMiddleware {
	return &ServiceAPIMiddleware{
		Logger: logger,
	}
}

func (m *ServiceAPIMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	rw.Header().Add("Accept-Charset", "utf-8")
	rw.Header().Add("Content-Type", "application/json")

	requestLogger := logging.CloneLogrusEntry(m.Logger)

	if userIP := cHttp.GetIPAdress(r); userIP != "" {
		requestLogger.Data["userip"] = userIP
	}
	// Set the logger on the request context
	requestContext := r.Context()
	requestContext = context.WithValue(requestContext, RequestLoggerContextKey, requestLogger)
	r = r.WithContext(requestContext)

	requestID := r.Header.Get(golibs.HeaderRequestIDName)
	if requestID != "" {
		requestLogger.Data[golibs.LogRequestIDKeyName] = requestID
		r.Header.Set(golibs.MetadataRequestIDName, requestID)
	}

	next(rw, r)
}
