package api

import (
	"io"
	"net/http"

	"github.com/Sirupsen/logrus"
)

// CombinedLoggingMiddleware is a middleware that adds Gorilla combined logging handler using the
// request logger from the request context.
type CombinedLoggingMiddleware struct {
	DefaultLogger          *logrus.Entry
	HTTPHandler            http.Handler
	CombinedLoggingHandler func(io.Writer, http.Handler) http.Handler
}

// NewCombinedLoggingMiddleware create a new CombinedLoggingMiddleware
func NewCombinedLoggingMiddleware(
	defaultLogger *logrus.Entry,
	httpHandler http.Handler,
	combineLogginHandler func(io.Writer, http.Handler) http.Handler,
) *CombinedLoggingMiddleware {
	return &CombinedLoggingMiddleware{
		DefaultLogger:          defaultLogger,
		HTTPHandler:            httpHandler,
		CombinedLoggingHandler: combineLogginHandler,
	}
}

func (m *CombinedLoggingMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	combinedLogger := m.DefaultLogger

	// Look for the request logger in the request context
	contextLogger := r.Context().Value(RequestLoggerContextKey)
	if contextLogger != nil {
		if requestLogger, ok := contextLogger.(*logrus.Entry); ok {
			combinedLogger = requestLogger
		}
	}

	m.CombinedLoggingHandler(io.Writer(combinedLogger.Writer()), m.HTTPHandler).ServeHTTP(rw, r)

	next(rw, r)
}
