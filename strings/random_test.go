package strings

import "testing"
import "github.com/stretchr/testify/assert"

func TestRand(t *testing.T) {
	assert := assert.New(t)

	// validate length
	length1 := 16
	rand1 := Rand(length1)
	assert.Len(rand1, length1)

	// validate non-identical
	rand2 := Rand(length1)
	assert.NotEqual(rand1, rand2)

	// validate big length
	length3 := 256
	rand3 := Rand(length3)
	assert.NotEqual(rand3, length3)
}

func BenchmarkRand(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Rand(32)
	}
	b.StopTimer()
	b.ReportAllocs()
}
