package strings

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSlugify(t *testing.T) {
	assert := assert.New(t)

	var testCases = []struct {
		in   string
		want string
	}{
		{"影師", "ying-shi"},
		{"  Dobroslaw     Zybort  ?", "dobroslaw-zybort"},
		{"Ala ma 6 kotów.", "ala-ma-6-kotow"},
		{"y̨Y̨", "yy"},
		{"źŹżŹ", "zzzz"},
		{"·/,:;`˜'\"", ""},
		{"2000–2013", "2000-2013"},
		{"style—not", "style-not"},
		{"test_slug", "test_slug"},
		{"jaja---lol-méméméoo--a", "jaja-lol-mememeoo-a"},
		{"This & that", "this-and-that"},
	}

	for index, st := range testCases {
		got := Slugify(st.in)
		assert.Equal(st.want, got, "%d. Make(%#v) = %#v; want %#v", index, st.in, got, st.want)
	}
}
