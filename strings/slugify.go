package strings

import (
	"bytes"
	"regexp"
	"strings"

	"github.com/rainycape/unidecode"
)

var (
	subs = map[rune]string{
		'"':  "",
		'\'': "",
		'’':  "",
		'‒':  "-", // figure dash
		'–':  "-", // en dash
		'—':  "-", // em dash
		'―':  "-",
		'&':  "and",
		'@':  "at",
	}

	regexpNonAuthorizedChars = regexp.MustCompile("[^a-z0-9-_]")
	regexpMultipleDashes     = regexp.MustCompile("-+")
)

// Slugify creates a slug from a given string
func Slugify(s string) string {
	slug := strings.TrimSpace(s)

	slug = SubstituteRune(slug, subs)

	// Process all non ASCII symbols
	slug = unidecode.Unidecode(slug)

	slug = strings.ToLower(slug)

	// Process all remaining symbols
	slug = regexpNonAuthorizedChars.ReplaceAllString(slug, "-")
	slug = regexpMultipleDashes.ReplaceAllString(slug, "-")
	slug = strings.Trim(slug, "-")

	return slug
}

// SubstituteRune substitutes string chars with provided rune
// substitution map. One pass.
func SubstituteRune(s string, sub map[rune]string) string {
	var buf bytes.Buffer
	for _, c := range s {
		if d, ok := sub[c]; ok {
			buf.WriteString(d)
		} else {
			buf.WriteRune(c)
		}
	}
	return buf.String()
}
