package crypto

import "testing"
import "github.com/stretchr/testify/assert"

func TestHashPassword(t *testing.T) {
	assert := assert.New(t)

	// assert correct length
	hash1, salt1 := HashPassword("dummy")
	assert.Len(hash1, PasswordHashBytes)
	assert.Len(salt1, PasswordSaltBytes)

	// assert random salt
	hash2, salt2 := HashPassword("dummy")
	assert.NotEqual(hash1, hash2)
	assert.NotEqual(salt1, salt2)
}

func TestHashPasswordWithSalt(t *testing.T) {
	assert := assert.New(t)

	// assert correct length
	hash1 := HashPasswordWithSalt("dummy", "salt")
	assert.Len(hash1, PasswordHashBytes)

	// cross check now
	hash2, salt2 := HashPassword("dummy")
	hash3 := HashPasswordWithSalt("dummy", salt2)
	assert.Equal(hash2, hash3)
}

func BenchmarkHashPassword(b *testing.B) {
	pass := "T~h1s#n_4v4ra-g4l0n5.}P4ss;0rd"
	for i := 0; i < b.N; i++ {
		HashPassword(pass)
	}
	b.StopTimer()
	b.ReportAllocs()
}
