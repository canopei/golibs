package crypto

import (
	"crypto/rand"
	"io"

	"golang.org/x/crypto/scrypt"
)

const (
	// PasswordSaltBytes represents the length of the generated salt
	PasswordSaltBytes = 32
	// PasswordHashBytes represents the length of the generated hash
	PasswordHashBytes = 64
)

// HashPassword generates a salt and computes the hash of a password
func HashPassword(password string) (hash string, salt string) {
	salt = generateSalt(PasswordSaltBytes)
	dk, _ := scrypt.Key([]byte(password), []byte(salt), 1<<14, 8, 1, PasswordHashBytes)

	return string(dk), salt
}

// HashPasswordWithSalt computes the hash of a password with the given salt
func HashPasswordWithSalt(password string, salt string) string {
	dk, _ := scrypt.Key([]byte(password), []byte(salt), 1<<14, 8, 1, PasswordHashBytes)

	return string(dk)
}

func generateSalt(length int) string {
	salt := make([]byte, length)
	io.ReadFull(rand.Reader, salt)

	return string(salt[:])
}
