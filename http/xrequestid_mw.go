package http

import (
	"crypto/rand"
	"encoding/hex"
	"net/http"
)

// DefaultRequestIDHeaderKey is the default header name
const DefaultRequestIDHeaderKey = "X-Request-Id"

// GenerateFunc is the func used by the middleware to generates the random string.
type GenerateFunc func(int) (string, error)

// XRequestIDMiddleware is a middleware that adds a random ID to the request X-Request-Id header
// Forked from https://github.com/pilu/xrequestid
type XRequestIDMiddleware struct {
	// Size specifies the length of the random length. The length of the result string is twice of n.
	Size int
	// Generate is a GenerateFunc that generates the random string. The default one uses crypto/rand
	Generate GenerateFunc
	// HeaderKey is the header name where the middleware set the random string. By default it uses the DefaultHeaderKey constant value
	HeaderKey string
}

// NewXRequestIDMiddleware returns a new XRequestIDMiddleware middleware instance. n specifies the length of the random length. The length of the result string is twice of n.
func NewXRequestIDMiddleware(n int) *XRequestIDMiddleware {
	return &XRequestIDMiddleware{
		Size:      n,
		Generate:  generateID,
		HeaderKey: DefaultRequestIDHeaderKey,
	}
}

func (m *XRequestIDMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	var err error

	// Check if already exists
	requestID := r.Header.Get(m.HeaderKey)
	if requestID == "" {
		requestID, err = m.Generate(m.Size)
		if err == nil {
			r.Header.Set(m.HeaderKey, requestID)
		}
	}

	// Always pass it to the response
	if requestID != "" {
		rw.Header().Set(m.HeaderKey, requestID)
	}

	next(rw, r)
}

func generateID(n int) (string, error) {
	r := make([]byte, n)
	_, err := rand.Read(r)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(r), nil
}
