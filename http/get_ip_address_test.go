package http

import (
	"testing"

	"net/http"

	"github.com/stretchr/testify/assert"
)

func TestGetIPAdress(t *testing.T) {
	emptyRequest := &http.Request{}

	// Empty string if no header exists
	ip := GetIPAdress(emptyRequest)
	assert.Empty(t, ip)

	// If we have remote addr with or without port, we should get it (only the IP)
	requestWithRemoteAddr := &http.Request{
		RemoteAddr: "0.0.0.0",
	}
	ip = GetIPAdress(requestWithRemoteAddr)
	assert.Equal(t, "0.0.0.0", ip)

	requestWithRemoteAddrWithPort := &http.Request{
		RemoteAddr: "0.0.0.0:1234",
	}
	ip = GetIPAdress(requestWithRemoteAddrWithPort)
	assert.Equal(t, "0.0.0.0", ip)

	// If we have multiple X-Forwarded-For IPs, we should get the first one.
	requestWithXForwardedFor := &http.Request{
		RemoteAddr: "0.0.0.0",
		Header: http.Header{
			"X-Forwarded-For": []string{"1.1.1.1", "2.2.2.2"},
		},
	}
	ip = GetIPAdress(requestWithXForwardedFor)
	assert.Equal(t, "1.1.1.1", ip)

	// If we have X-Real-Ip we should get that one
	requestWithXRealIP := &http.Request{
		RemoteAddr: "0.0.0.0",
		Header: http.Header{
			"X-Real-Ip": []string{"3.3.3.3"},
		},
	}
	ip = GetIPAdress(requestWithXRealIP)
	assert.Equal(t, "3.3.3.3", ip)

	// If we have both X-Forwarded-For and X-Real-IP we should get X-Real-IP
	requestWithXRealIPAndXForwardedFor := &http.Request{
		RemoteAddr: "0.0.0.0",
		Header: http.Header{
			"X-Forwarded-For": []string{"1.1.1.1", "2.2.2.2"},
			"X-Real-Ip":       []string{"3.3.3.3"},
		},
	}
	ip = GetIPAdress(requestWithXRealIPAndXForwardedFor)
	assert.Equal(t, "3.3.3.3", ip)
}

// GetIPAdress retrieves the remote IP address from a http.Request
// func GetIPAdress(r *http.Request) string {
// 	var ipAddress string

// 	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
// 		for _, ip := range strings.Split(r.Header.Get(h), ",") {
// 			if ip != "" {
// 				realIP := net.ParseIP(strings.Replace(ip, " ", "", -1))
// 				ipAddress = realIP.String()
// 			}
// 		}
// 	}

// 	if ipAddress == "" {
// 		ip, _, err := net.SplitHostPort(r.RemoteAddr)
// 		if err != nil {
// 			ipAddress = ""
// 		}

// 		userIP := net.ParseIP(ip)
// 		if userIP == nil {
// 			ipAddress = ""
// 		}

// 		ipAddress = userIP.String()
// 	}

// 	return ipAddress
// }
