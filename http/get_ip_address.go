package http

import (
	"net"
	"net/http"
	"strings"
)

// GetIPAdress retrieves the remote IP address from a http.Request
func GetIPAdress(r *http.Request) string {
	var ipAddress string
	var err error

	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		for _, ip := range strings.Split(r.Header.Get(h), ",") {
			if ip != "" {
				realIP := net.ParseIP(strings.Replace(ip, " ", "", -1))
				ipAddress = realIP.String()
			}
		}
	}
	if ipAddress != "" {
		return ipAddress
	}

	if r.RemoteAddr != "" {
		ip := r.RemoteAddr
		if strings.Contains(ip, ":") {
			ip, _, err = net.SplitHostPort(ip)
			if err != nil {
				return ipAddress
			}
		}

		userIP := net.ParseIP(ip)
		if userIP != nil {
			ipAddress = userIP.String()
		}
	}

	return ipAddress
}
