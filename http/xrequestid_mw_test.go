package http

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestXRequestID(t *testing.T) {
	assert := assert.New(t)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	middleware := NewXRequestIDMiddleware(16)
	middleware.Generate = func(n int) (string, error) { return "test-id", nil }
	middleware.ServeHTTP(recorder, req, func(w http.ResponseWriter, r *http.Request) {})

	id := req.Header.Get(middleware.HeaderKey)
	assert.Equal("test-id", id)

	responseID := recorder.HeaderMap.Get(middleware.HeaderKey)
	assert.Equal("test-id", responseID)
}

func TestXRequestIDWithExisting(t *testing.T) {
	assert := assert.New(t)

	middleware := NewXRequestIDMiddleware(16)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)
	req.Header.Set(middleware.HeaderKey, "existing-id")

	middleware.Generate = func(n int) (string, error) { return "test-id", nil }
	middleware.ServeHTTP(recorder, req, func(w http.ResponseWriter, r *http.Request) {})

	id := req.Header.Get(middleware.HeaderKey)
	assert.Equal("existing-id", id)

	responseID := recorder.HeaderMap.Get(middleware.HeaderKey)
	assert.Equal("existing-id", responseID)
}

func TestXRequestIDDefaultGenerate(t *testing.T) {
	assert := assert.New(t)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	middleware := NewXRequestIDMiddleware(16)
	middleware.ServeHTTP(recorder, req, func(w http.ResponseWriter, r *http.Request) {})

	id := req.Header.Get(middleware.HeaderKey)
	assert.Len(id, 2*16)
}
