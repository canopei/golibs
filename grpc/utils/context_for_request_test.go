package utils

import (
	"context"
	"net/http"
	"testing"

	"bitbucket.org/canopei/golibs"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"
)

func TestGetContextForRequest(t *testing.T) {
	ctx := context.Background()
	req := &http.Request{}

	// Without request ID header
	newCtx := GetContextForRequest(ctx, req)
	MD, _ := metadata.FromOutgoingContext(newCtx)
	_, exists := MD[golibs.MetadataRequestIDName]
	assert.False(t, exists)

	// With request ID header
	req.Header = http.Header{}
	req.Header.Add(golibs.HeaderRequestIDName, "reqid")

	newCtx = GetContextForRequest(ctx, req)
	MD, _ = metadata.FromOutgoingContext(newCtx)
	reqID, exists := MD[golibs.MetadataRequestIDName]
	assert.True(t, exists)
	// The values in metadata are slices
	assert.Equal(t, []string{"reqid"}, reqID)

	// With empty request ID header
	req.Header = http.Header{}
	req.Header.Add(golibs.HeaderRequestIDName, "")
	newCtx = GetContextForRequest(ctx, req)
	MD, _ = metadata.FromOutgoingContext(newCtx)
	_, exists = MD[golibs.MetadataRequestIDName]
	assert.False(t, exists)
}
