package utils

import (
	"github.com/Sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// InternalError logs and creates a gRPC unknown error
func InternalError(logger *logrus.Entry, err error, format string, a ...interface{}) error {
	logger.Errorf("%s: %v", format, err)
	return grpc.Errorf(codes.Unknown, format, a...)
}
