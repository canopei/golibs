package utils

import (
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"

	"bitbucket.org/canopei/golibs"
	"google.golang.org/grpc/metadata"
)

// GetContextLogger builds an instance of a logger for the current context using an original logger
// It adds a requestid Field if the gRPC request has this metadata header
func GetContextLogger(ctx context.Context, originalLogger *logrus.Entry) *logrus.Entry {
	log := logrus.New()
	log.Formatter = originalLogger.Logger.Formatter
	log.Level = originalLogger.Logger.Level
	log.Hooks = originalLogger.Logger.Hooks
	log.Out = originalLogger.Logger.Out
	newLogger := log.WithFields(originalLogger.Data)

	headers, ok := metadata.FromIncomingContext(ctx)
	if ok {
		if requestID, ok := headers[golibs.MetadataRequestIDName]; ok {
			newLogger.Data[golibs.LogRequestIDKeyName] = requestID[0]
		}
	}

	return newLogger
}
