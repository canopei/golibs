package utils

import (
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"

	"testing"

	"bitbucket.org/canopei/golibs"
	"github.com/stretchr/testify/assert"
)

func TestGetContextLogger(t *testing.T) {
	logger := logrus.WithFields(logrus.Fields{})
	// setup some custom formatter
	customFormatter := new(logrus.TextFormatter)
	customFormatter.FullTimestamp = true
	logger.Logger.Formatter = customFormatter
	// set some log level
	logger.Logger.Level = logrus.DebugLevel

	ctx := context.Background()

	newLogger := GetContextLogger(ctx, logger)
	// These must be preserved
	assert.Equal(t, logger.Logger.Formatter, newLogger.Logger.Formatter)
	assert.Equal(t, logger.Logger.Hooks, newLogger.Logger.Hooks)
	assert.Equal(t, logger.Logger.Level, newLogger.Logger.Level)

	// No requestid in the ctx metadata should result in no request id logger.Data
	_, ok := newLogger.Data[golibs.LogRequestIDKeyName]
	assert.False(t, ok)

	// Test with request ID in the context metadata
	header := metadata.New(map[string]string{
		golibs.MetadataRequestIDName: "reqid",
	})
	newCtx := metadata.NewIncomingContext(ctx, header)

	newLogger = GetContextLogger(newCtx, logger)
	requestID, ok := newLogger.Data[golibs.LogRequestIDKeyName]
	assert.True(t, ok)
	assert.Equal(t, "reqid", requestID)
}
