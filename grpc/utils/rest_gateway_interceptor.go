package utils

import (
	"net/http"
)

// RestGatewayInterceptor can override the original writer
type RestGatewayInterceptor struct {
	origWriter http.ResponseWriter
	overridden bool
}

// WriteHeader overrides the response
func (i *RestGatewayInterceptor) WriteHeader(rc int) {
	switch rc {
	case 404:
		i.origWriter.WriteHeader(404)
	case 204:
		i.origWriter.WriteHeader(204)
	default:
		i.origWriter.WriteHeader(rc)
		return
	}
	// if the default case didn't execute (and return) we must have overridden the output
	i.overridden = true
}

// Write overrides the writer if we already have overriden the response in WriteHeader
func (i *RestGatewayInterceptor) Write(b []byte) (int, error) {
	if !i.overridden {
		// if we have an empty response, set code to 204
		if string(b) == "{}" || string(b) == "" {
			i.origWriter.WriteHeader(204)
		} else {
			i.origWriter.Write(b)
		}
	}

	// Return nothing if we've overriden the response.
	return 0, nil
}

// Header fetches the headers map
func (i *RestGatewayInterceptor) Header() http.Header {
	return i.origWriter.Header()
}

// RestGatewayResponseInterceptor defines an intercepting handler
func RestGatewayResponseInterceptor(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w = &RestGatewayInterceptor{origWriter: w}

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
