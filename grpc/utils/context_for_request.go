package utils

import (
	"context"
	"net/http"

	"bitbucket.org/canopei/golibs"
	"google.golang.org/grpc/metadata"
)

// GetContextForRequest adds request ID metadata to the gRPC context from a HTTP request
func GetContextForRequest(ctx context.Context, req *http.Request) context.Context {
	requestID := req.Header.Get(golibs.HeaderRequestIDName)
	if requestID != "" {
		header := metadata.New(map[string]string{
			golibs.MetadataRequestIDName: requestID,
		})
		newCtx := metadata.NewOutgoingContext(ctx, header)

		return newCtx
	}

	return ctx
}
