package slices

// Scontains checks if a given string exists in a strings slice
func Scontains(slice []string, search string) bool {
	for _, value := range slice {
		if value == search {
			return true
		}
	}
	return false
}

// Icontains32 checks if a given int32 exists in a int32s slice
func Icontains32(slice []int32, search int32) bool {
	for _, value := range slice {
		if value == search {
			return true
		}
	}
	return false
}

// Icontains64 checks if a given int64 exists in a int64s slice
func Icontains64(slice []int64, search int64) bool {
	for _, value := range slice {
		if value == search {
			return true
		}
	}
	return false
}
