package slices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestScontains(t *testing.T) {
	tests := []struct {
		In    []string
		Check string
		Out   bool
	}{
		{[]string{"foo", "123", "foo", ""}, "123", true},
		{[]string{"foo", "123", ""}, "bar", false},
		{[]string{"foo", "123", ""}, "", true},
		{[]string{"foo", "123"}, "", false},
		{[]string{}, "123", false},
		{[]string{}, "", false},
	}

	for _, test := range tests {
		result := Scontains(test.In, test.Check)
		assert.Equal(t, test.Out, result)
	}
}

func TestIcontains32(t *testing.T) {
	tests := []struct {
		In    []int32
		Check int32
		Out   bool
	}{
		{[]int32{1, 2, 2, 3, -4}, -4, true},
		{[]int32{1, 2, 2, 3, -4}, 4, false},
		{[]int32{0, 2, 2, 3, -4}, 0, true},
		{[]int32{1, -3}, 0, false},
		{[]int32{}, -4, false},
		{[]int32{}, 0, false},
	}

	for _, test := range tests {
		result := Icontains32(test.In, test.Check)
		assert.Equal(t, test.Out, result)
	}
}

func TestIcontains64(t *testing.T) {
	tests := []struct {
		In    []int64
		Check int64
		Out   bool
	}{
		{[]int64{1, 2, 2, 3, -4}, -4, true},
		{[]int64{1, 2, 2, 3, -4}, 4, false},
		{[]int64{0, 2, 2, 3, -4}, 0, true},
		{[]int64{1, -3}, 0, false},
		{[]int64{}, -4, false},
		{[]int64{}, 0, false},
	}

	for _, test := range tests {
		result := Icontains64(test.In, test.Check)
		assert.Equal(t, test.Out, result)
	}
}
